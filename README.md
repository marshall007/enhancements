# GitLab Enhancement Proposals

> :alert: This project is currently proof-of-concept only. It is not an
> authoritative source of release information or planned functionality.

Enhancement proposal tracking for GitLab. This project is owned by
Quality, but individual enhancements are owned by their
respective Product Stages.

This project contains enhancement proposals for planned changes to GitLab.
An enhancement may take multiple releases to complete and may be
tracked as backlog items before work begins. An enhancement may be merged
after approval from at least one Product Stage.

## Is My Thing an Enhancement?

An enhancement is anything that:

- a blog post would be written about after its release
- requires coordination across multiple functions
- could impact overall system stabaliity
- requires significant effort or changes GitLab in a significant way
  (ex. something that would take 10 person-weeks to implement,
  introduce or redesign a system component, or introduces API changes)
- impacts the operation of GitLab substantially, will require manual upgrades,
  or introduce special handling across distributions and deployments

It is unlikely an enhancement if it is:

- fixing a flaky test
- minor refactoring of code
- performance improvements, which are only visible to users as faster API operations
- upgrading API-compatible versions of dependencies

## Why Are Enhancements Tracked

There is a cost associated with introducing new API surface area, especially if
it does not scale with long-term design goals. Every additional user-facing
config file, API endpoint, and group/project setting adds complexity to the
system. By tracking enhancement proposals, we can efficiently communicate
changes across the team and forward in time.

After enhancements are
[graduated to beta](https://about.gitlab.com/handbook/product/gitlab-the-product/#open-beta),
users expect functionality to be stable for an extended period of time.
Therefore, we hold new enhancements to a high standard of conceptual integrity
and require consistency with other parts of the system, thorough testing,
appropriate defaults and rate limits, and complete documentation.

In many cases, no single Product group (let alone a single person) can track
whether all of these requirements are met.

## Labels

| Label Name | Purpose |
| ------ | ------ |
| `stage-*` | Denotes the Product Stage(s) which owns this enhancement (e.g., `stage-enablement`) |
| `type::` | Denotes the [work type classification](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) (e.g. `type::maintainence`) |
| `readiness::` | Denotes the [stage](https://about.gitlab.com/handbook/product/gitlab-the-product/#alpha-beta-ga) in the feature graduation process (e.g. `readiness::alpha`) |

## Prior Art

This project and its documentation borrow heavily from the
[Kubernetes Enhancement Proposal (KEP)](https://github.com/kubernetes/enhancements)
process.
