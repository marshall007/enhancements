# GitLab Design Principles

## Reusable

1. Existing components and features should be reused when possible instead of adding new ones.
Before proposing a feature, try solving the problem with existing features first.
1. Prefer solutions that build on top of public APIs over those coupled to internal implementation details.
1. Maintain [parity across different distributions and deployment architectures](https://about.gitlab.com/handbook/product/product-principles/#parity-between-saas-and-self-managed-deployments).

## Simple

1. Enhancements should contain only the bare minimum and simplest features needed to meet the largest number of use cases.
1. Prefer a [simple](https://www.infoq.com/presentations/Simple-Made-Easy/) solution that solves most use cases to a
complex solution that solves all use cases (can be revisited later).
1. New features should be consistent with existing components, in structure and behavior, to make learnability,
trialability and adoption easy.
1. [Avoid adding new configuration](https://about.gitlab.com/handbook/product/product-principles/#configuration-principles)
and any other form of surface area to APIs unless absolutely necessary.

## Flexible

1. To keep GitLab flexible, avoid being overly opinionated.
1. We are a comprehensive toolchain, but functionality can be consumed a la carte. Avoid preventing users from using
their own solutions in lieu of built-in functionality.
1. [Integrate other applications thoughtfully](https://about.gitlab.com/handbook/product/product-principles/#integrate-other-applications-thoughtfully)
and prefer solutions that enable users to build such integrations themselves.
1. Avoid implementing your own expression syntax and domain-specific languages (DSLs). When required, prefer existing
languages which are widely used and include supporting development tools.

## Innovative

1. Build for the [next generation](https://about.gitlab.com/handbook/product/product-principles/#next-generation).
1. Don't just fill product gaps that exist today, focus on where competitors will be tomorrow and build that.
1. Focus on solutions that target [modern teams](https://about.gitlab.com/handbook/product/product-principles/#modern-first)
and [cloud-native](https://about.gitlab.com/handbook/product/product-principles/#cloud-native-first) platforms
before you worry about legacy use cases.
