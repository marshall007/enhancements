---
id: 1
status: pending
author: "@marshall007"
collaborators: []
responsible-sig: "sig::api"
milestone:
  alpha: "%15.5"
  beta: "%15.8"
  ga: "%16.0"
---

<!--
**Note:** Please remove comment blocks for sections you've filled in.
When your GLEP is complete, all of these comment blocks should be removed.

To get started with this template:

- [ ] **Fill out this file as best you can.**
  At minimum, you should fill in the "Summary", and "Motivation" sections.
  These should be easy if you've preflighted the idea of the GLEP with the
  appropriate Working Group.
- [ ] **Create a MR for this GLEP.**
  Assign it to people in the Working Group that are sponsoring this process.
- [ ] **Merge early and iterate.**
  Avoid getting hung up on specific details and instead aim to get the goals of
  the GLEP clarified and merged quickly. The best way to do this is to just
  start with the high-level sections and fill out details incrementally in
  subsequent MRs.

Just because a GLEP is merged does not mean it is complete or approved. Any GLEP
marked as a `proposed` is a working document and subject to change.

When editing GLEPS, aim for tightly-scoped, single-topic MRs to keep discussions
focused. If you disagree with what is already in a document, open a new MR
with suggested changes.

If there are new details that belong in the GLEP, edit the GLEP. Once a
feature has become "implemented", major changes should get new GLEPs.

The canonical place for the latest set of instructions (and the likely source
of this file) is [here](/tools/template/glep-template.md).
-->

# Distributed Policy Enforcement

[[_TOC_]]

## Summary

GitLab consists of multiple services which are composed together to form a cohesive application.
We very deliberately treat this as an implementation detail and not something that users need to be aware of.
As a result, users expect that application configuration, plan limits, and group/project settings are
applied consistently across the entire product.

This proposal describes a means of defining policies in one place and distributing both policies
and the data necessary to evaluate them to all services.

<!--
This section is incredibly important for producing high quality user-focused
documentation such as release notes or a development roadmap. It should be
possible to collect this information before implementation begins in order to
avoid requiring implementors to split their attention between writing release
notes and implementing the feature itself.

A good summary is probably at least a paragraph in length.

Both in this section and below, follow the guidelines of the
[documentation style guide](https://docs.gitlab.com/ee/development/documentation/styleguide/).
In particular, wrap lines to a reasonable length, to make it easier for
reviewers to cite specific portions, and to minimize diff churn on updates.
-->

## Motivation

Users expect things file size limits, IP allowlists, and usage quotas to be applied consistently.
Both operators and users need visibility into utilization and the evaluation of policy decisions.

<!--
This section is for explicitly listing the motivation, goals and non-goals of
this GLEP. Describe why the change is important and the benefits to users.

The motivation section can optionally provide links to issues that demonstrate
interest in a GLEP within the wider GitLab community.

Links to documentation for competing products and services is also encouraged in
cases where they demonstrate clear gaps in the functionality GitLab provides.
-->

### Goals

- Decouple policy logic from application code
- A consistent way to manage policies and limits
- Policies should be defined in one place
- Consistent enforcement of policies across all services by default
- Automated logging of policy decisions
- Support both passive and active policy enforcement
- Support hierarchical configurations (`Application` -> `Plan` -> `Namespace` -> `Project`)

<!--
List the specific goals of the GLEP.
- What is it trying to achieve?
- How will we know that this has succeeded?
-->

### Non-Goals

- Make every limit configurable at every level
- A UI for configuring or viewing policies and limits

<!--
Listing non-goals helps to focus discussion and make progress.
- What is out of scope for this GLEP?
-->

### Use Cases

<!--
Describe the concrete improvement specific groups of users will see if the
Motivations in this doc result in a fix or feature.

Consider the user's:
- [persona][https://about.gitlab.com/handbook/product/personas/]
- experience - what workflows are enhanced if this problem is solved?
- impact - what is the likelihood a typical user of this persona would take advantage of the functionality?
-->

## Proposal

<!--
This is where we get down to the specifics of what the proposal actually is, but keep it simple!
This should have enough detail that reviewers can understand exactly what you're proposing, but should not include
things like API designs or implementation. The "Design Details" section below is for the real nitty-gritty.
-->

## Design Details

An [Open Policy Agent](https://openpolicyagent.org/) sidecar will run alongside each service. More specifically,
we will likely deploy a sidecar alongside _each instance_ of every service. Rather than each service implementing
policy evaluation logic in the application code, the service will make requests to the OPA sidecar where the policy
will be evaluated.

```mermaid
flowchart TB
  OPA[(OPA Bundle Server)]
  ES[(Decision Logs)]

  subgraph fleet ["Runner Fleet"]
  runner1[Runner]-.-s1[["OPA Sidecar"]]
  runner2[Runner]-.-s2[["OPA Sidecar"]]
  runner3[Runner]-.-s3[["OPA Sidecar"]]
  end

  subgraph gitlab.com
  rails_api[Rails]-.-s4[["OPA Sidecar"]]
  workhorse[Workhorse]-.-s5[["OPA Sidecar"]]
  gitaly[Gitaly]-.-s6[["OPA Sidecar"]]
  end

  subgraph registry.gitlab.com
  registry[Container Registry]-.-s7[["OPA Sidecar"]]
  end

  subgraph kas.gitlab.com
  kas[KAS]-.-s8[["OPA Sidecar"]]
  end

  subgraph customers.gitlab.com
  customers_dot[CustomersDot]
  end

  OPA-->s1 & s2 & s3 & s4 & s5 & s6 & s7 & s8-->ES

  app_config([Application Config])
  plan_config([Plan Config])

  rails_api-->|Namespace/Group/Project Settings|OPA
  customers_dot-->|Customer Plans|OPA

  app_config & plan_config-->OPA
```

#### ~~Four~~ Five Layers of Rate Limiting

We currently have four layers of rate limiting, as outlined in
[the rate limiting runbook docs](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/rate-limiting):

1. CloudFlare
1. HAProxy
1. Application (`RackAttack`)
1. Application (`ApplicationRateLimiter`)
1. {+ Application (various satellite service implementations) +}

There are several linked issues in that document that describe efforts to consolidate in both directions
(i.e. [HAProxy -> CloudFlare](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/9709),
[HAProxy -> Application](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/632),
and/or consolidation of `RackAttack`/`ApplicationRateLimiter`).

What is not described in the runbook docs are the additional mechanisms in place for satellite services.
These services are either proxied through the Rails Application or implement their own rate limiting using
their own application configuration.

#### ~~Five Layers of~~ Unified Rate Limiting

The consolidation of application rate limiting across all services (i.e. layers 3-5) is described by the
[diagram in the design details section](#design-details). This proposal readily addresses rate limiting
at the edge as well (i.e. CDN + LB layers 1-2).

##### HAProxy

HAProxy lacks a mechanism for calling an external service. If we wish to enforce Rego policy at the
load balancer level, we will need to switch to a modern edge proxy (like those based on Envoy).
For more detail, see the Envoy documentation for
[External Authorization filters](https://www.envoyproxy.io/docs/envoy/v1.10.0/intro/arch_overview/ext_authz_filter.html).

##### CloudFlare

However, OPA also supports [compiling Rego policies to WebAssembly](https://www.openpolicyagent.org/docs/latest/wasm/).
These WASM modules can be deployed as [CloudFlare Workers](https://workers.cloudflare.com) and run at the edge.

This approach statically compiles the Rego policies into machine code, which means it cannot take advantage of dynamic
policy bundle distribution, but this is still far better than the opaque CDN and HAProxy configurations we have today.
Policies would be authored in same format, using the same tooling at the CDN, LB, and application level.

![](https://raw.githubusercontent.com/open-policy-agent/contrib/813150e453ae342d36d23ed7d4de1d55b5f638ed/wasm/cloudflare-worker/docs/CF-worker.png)

See [`open-policy-agent/contrib/wasm/cloudflare-worker`](https://github.com/open-policy-agent/contrib/blob/main/wasm/cloudflare-worker)
as an example.

#### Example Policy Evaluation

Services will make requests to the sidecars using a well-known input format. At minimum, the input should include
session and user metadata from the current request context, the relevant namespace/group/project ID, and the action
that is being performed.

OPA will return a simple `allow` boolean representing the decision as well as arbitrary metadata
related to the outcome which may be relevant to the end user (ex. `X-RateLimit-*` headers on HTTP responses).

> **TODO:** formally specify the input data we should expect from the request context. The following examples
> are not finalized.

```json
// POST http://localhost:8181/v1/data/actions
{
  "input": {
    "action": "issues.create",
    "namespace_id": 100,
    "project_id": 92351,
    "session": {
      "ip": "...",
      "user": "marshall007",
      "auth_method": "<personal_access_token|ssh_key|ci_job_token>",
      "two_factor_auth": true,
      "external_sso": true
    }
  }
}
```

```json
{
  "result": {
    "allow": true,
    "limit": {
      "count": 1,
      "key": "issues.create:user:marshall007",
      "threshold": 5,
      "ttl_ns": 60000000000
    }
  }
}
```

##### Bundle Server

Implementing [the Bundle Service API](https://www.openpolicyagent.org/docs/latest/management-bundles/) will be the most
challenging aspect of this proposal.

The [bundle file format](https://www.openpolicyagent.org/docs/latest/management-bundles/#bundle-file-format) itself
is trivial. Our policies and application config data will be relatively static and easy to generate and regenerate
bundles for. However, we need to account for the dynamic data as well:

- customer subscription plans and addons
- admin and namespace settings
- group/project settings

The bundles containing all relevant group and project settings will likely be quite large, so we should plan to
support [delta bundles](https://www.openpolicyagent.org/docs/latest/management-bundles/#delta-bundle-file-format)
and HTTP caching using the ETag and `If-None-Match` headers as described in the
[Delta Bundle FAQ](https://www.openpolicyagent.org/docs/latest/management-bundles/#delta-bundle-faq).

<!--
This section should contain enough information that the specifics of your change are understandable.
This may include API specs (though not always required) or even code snippets. If there's any ambiguity about HOW your
proposal will be implemented, this is the place to discuss them.

If it's helpful to include workflow diagrams or any other related images. Diagrams authored in GitLab flavored markdown
are prefered. In cases where that is not feasible, images should be placed under `images/` in the same directory
as the README.md for the proposal.
-->

## Design Evaluation

<!--
How does this proposal affect the api conventions, reusability, simplicity, flexibility and conformance of GitLab,
as described in [design principles](/DESIGN.md)
-->

### Reusability

##### Audit Events

This approach could vastly simplify the implementation of
[Audit Events](https://docs.gitlab.com/ee/administration/audit_event_streaming.html).
The OPA sidecars will be configured to write their decision logs to a remote endpoint. Currently audit events are
triggered in an ad-hoc fashion throughout the application, and it is up to individual teams to approapriately dispatch
audit events from application code. By capturing the decision logs from OPA sidecars, we ensure that there is an
auditable record for virtually every event in the system.

##### Compliance and Governence

While not in scope of the current proposal, introducing OPA into the stack presents future opportunities for robust
product integrations and user-facing capablities. There are numerous opportunities to consolidate existing
compliance-related functionality like:

- [External Pipeline Validation](https://docs.gitlab.com/ee/administration/external_pipeline_validation.html)
- [Scan Execution Policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-execution-policies.html)
- [Scan Result Policies](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html)

In the future, we could allow customers to apply custom Rego policy to virtually anything. This could include project
settings, CI pipeline definitions, Kubernetes deployments, Terraform plans, etc.

<!--
[reusable]: /DESIGN.md#reusable

- Are there existing features related to the proposed features?
- Are there opportunities to leverage or extend existing components of GitLab?
-->

### Simplicity

From an operator's perspective, this architecture makes it much easier to configure GitLab. Instead of configuring
limits for each of the services individually, our SREs and self-hosted customers can define everything in one place.

The overall architecture is simple. It will provide an opportunity to more fully decouple satellite services from the
Rails monolith. We will have to propagate changes to customer plans and group/project settings to the bundle server,
which is complicated, but this is an intermediary problem and things will be even simpler once we fully establish the
new single source of truth.

<!--
[simple]: /DESIGN.md#simple

- How does this proposal affect the user experience?
- What’s the current user experience without the feature and how challenging is it?
- What will be the user experience with the feature? How would it have changed?
- Does this proposal contain the bare minimum change needed to solve for the use cases?
- Are there any implicit behaviors in the proposal? Would users expect these implicit behaviors or would they be
surprising? Are there security implications for these implicit behaviors?
-->

### Flexibility

Rego policies are incredibly flexible and should allow us to model a large number of policy enforcement use cases. OPA
is also quite extensible through the Go SDK and upcoming WASM support.

<!--
[flexible]: /DESIGN.md#flexible

- Are we coupling two or more GitLab components in this proposal?
- If so, are they loosely coupled (use public APIs) or tightly coupled (rely on internal APIs or implementation details)?
- Are there opinionated choices being made in this proposal? If so, are they necessary and can users extend it with
their own choices?
-->

### Performance

The distributed architecture with sidecars running alongside each service lends itself well to performance. It is also
quite resiliant to various failure modes. In the worst case, when the bundle server is completely down, sidecars will
continue to be able to enforce policy based on the last known configuration.

<!--
(optional)

Consider which use cases are impacted by this change and what are their
performance requirements.
-->

### Risks and Mitigations

<!--
What are the risks of this proposal and how do we mitigate? Think broadly.
For example, consider both security and how this will impact the larger
GitLab ecosystem. Consider including folks that also work outside the SIGs.
-->

### Drawbacks

<!--
Why should this GLEP _not_ be implemented?
-->

## Alternatives

<!--
What other approaches did you consider and why did you rule them out? These do
not need to be as detailed as the proposal, but should include enough
information to express the idea and why it was not acceptable.
-->

## Implementation Plan

<!--
What are the implementation phases or milestones? Taking an incremental approach
makes it easier to review associated merge requests.
-->

### Infrastructure Needed

<!--
(optional)

Examples include a new group or project repo in GitLab, changes to distribution,
packaging, and/or infrastructure changes. Listing these here allows a SIG to
get the process for these resources started right away.
-->

### Upgrade and Migration Strategy

<!--
(optional)

Use this section to detail whether this feature needs an upgrade or
migration strategy. This is especially useful when we modify a
behavior or add a feature that may replace and deprecate a current one.
-->

## References

<!--
(optional)

Use this section to add links to GitLab issues, other GLEPs, docs in GitLab
shared drive, examples, etc. This is useful to refer back to any other related links
to get more details.
-->
