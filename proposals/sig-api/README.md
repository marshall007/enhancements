## SIG API

The current source of truth for the SIG API charter is [the handbook][sig-api].

<!-- TODO: update links once after this is merged: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104378 -->
[sig-api]: https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/handbook/gb/add-sigs/sites/uncategorized/source/company/team/structure/sigs/sig-api/index.html.md
