## SIG Contrib

The current source of truth for the SIG Contrib charter is [the handbook][sig-contrib].

<!-- TODO: update links once after this is merged: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104378 -->
[sig-contrib]: https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/handbook/gb/add-sigs/sites/uncategorized/source/company/team/structure/sigs/sig-contrib/index.html.md
