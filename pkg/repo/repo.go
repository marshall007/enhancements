package repo

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/marshall007/enhancements/pkg/util"

	"github.com/hashicorp/go-retryablehttp"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v2"
)

var (
	DefaultProjectID    = "marshall007/enhancements"
	DefaultProposalDir  = "proposals"
	DefaultTemplatePath = "template.md"
)

type Options struct {
	ProposalPath string
	TemplatePath string
	ProjectId    string
}

type Repo struct {
	config *Options
	client *gitlab.Client
}

func New(config *Options) *Repo {
	client, _ := gitlab.NewClient(
		os.Getenv("GITLAB_TOKEN"),
		gitlab.WithResponseLogHook(func(l retryablehttp.Logger, r *http.Response) {
			logrus.Debugf("go-gitlab response:", r)
		}),
	)

	return &Repo{
		client: client,
		config: config,
	}
}

func (r *Repo) CreatePlaceholderMR(title string) (*gitlab.MergeRequest, error) {
	branch := util.Slug(title)
	data, _, err := r.client.Branches.CreateBranch(
		r.config.ProjectId,
		&gitlab.CreateBranchOptions{
			Branch: gitlab.String(branch),
			Ref:    gitlab.String("main"),
		},
	)
	if err != nil {
		return nil, err
	}

	mr, _, err := r.client.MergeRequests.CreateMergeRequest(
		r.config.ProjectId,
		&gitlab.CreateMergeRequestOptions{
			Title:              gitlab.String(title),
			SourceBranch:       gitlab.String(data.Name),
			TargetBranch:       gitlab.String("main"),
			RemoveSourceBranch: gitlab.Bool(true),
		},
	)

	return mr, err
}

func (r *Repo) GetExistingMR(id int) (*gitlab.MergeRequest, error) {
	mr, _, err := r.client.MergeRequests.GetMergeRequest(
		r.config.ProjectId,
		id,
		&gitlab.GetMergeRequestsOptions{},
	)

	return mr, err
}

func (r *Repo) GetUsers(usernames []string) map[string]*gitlab.User {
	result := make(map[string]*gitlab.User)

	for _, u := range usernames {
		u = strings.TrimPrefix(u, "@")

		list, _, err := r.client.Users.ListUsers(
			&gitlab.ListUsersOptions{
				Username: gitlab.String(u),
			},
		)

		if err != nil {
			logrus.Error(err)
		} else if len(list) == 1 {
			result[u] = list[0]
		}
	}

	return result
}

func (r *Repo) ReadTemplate() ([]byte, error) {
	data, _, err := r.client.RepositoryFiles.GetRawFile(
		r.config.ProjectId,
		r.config.TemplatePath,
		&gitlab.GetRawFileOptions{},
	)
	return data, err
}

func (r *Repo) PopulateTemplate(e interface{}) ([]byte, error) {
	builder := &bytes.Buffer{}

	builder.WriteString("---\n")

	meta, err := yaml.Marshal(e)
	if err != nil {
		return []byte{}, err
	}

	builder.Write(meta)
	builder.WriteString("---\n")

	contents, err := r.ReadTemplate()
	if err != nil {
		return []byte{}, err
	}

	builder.Write(contents)
	builder.WriteString("\n")

	return builder.Bytes(), nil
}

func (r *Repo) WriteProposal(branch, path, contents string) error {
	fullpath := filepath.Join(r.config.ProposalPath, path)
	logrus.Infof("Writing new proposal to %s", fullpath)

	_, _, err := r.client.RepositoryFiles.CreateFile(
		r.config.ProjectId,
		fullpath,
		&gitlab.CreateFileOptions{
			Branch:        gitlab.String(branch),
			CommitMessage: gitlab.String(fmt.Sprintf("adding \"%s\"", path)),
			Content:       gitlab.String(contents),
		},
	)

	return err
}

// Example: https://gitlab.com/-/ide/project/marshall007/enhancements/merge_requests/4
func (r *Repo) WebIDELink(mergeRequestId int) *url.URL {
	link := r.client.BaseURL()

	link.Path = fmt.Sprintf("/-/ide/project/%s/merge_requests/%d", r.config.ProjectId, mergeRequestId)

	return link
}

// Example: https://gitlab.com/marshall007/enhancements/-/branches?state=all&search=cli-mr-creation-test
func (r *Repo) BranchesLink(search string) *url.URL {
	link := r.client.BaseURL()

	query := url.Values{}
	query.Set("state", "all")
	query.Set("search", search)

	link.Path = fmt.Sprintf("/%s/-/branches", r.config.ProjectId)
	link.RawQuery = query.Encode()

	return link
}
