package util

import (
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

// Default program to fallback to, nano is typical default on Linux
const DefaultEditor = "nano"

// PreferredEditorResolver is a function that returns an editor that the user prefers to use, such as the configured
// `$EDITOR` environment variable.
type PreferredEditorResolver func() string

type TempFileResolver func() (*os.File, error)

// GetPreferredEditorFromEnvironment returns the user's editor as defined by the `$EDITOR` environment variable, or
// the `DefaultEditor` if it is not set.
func GetPreferredEditorFromEnvironment() string {
	editor := os.Getenv("EDITOR")

	if editor == "" {
		return DefaultEditor
	}

	return editor
}

func resolveEditorArguments(executable string, filename string) []string {
	args := []string{filename}

	if strings.Contains(executable, "Visual Studio Code.app") {
		args = append([]string{"--wait"}, args...)
	}

	// Other common editors
	//
	// ...
	//

	return args
}

// OpenFileInEditor opens filename in a text editor.
func OpenFileInEditor(filename string, resolveEditor PreferredEditorResolver) error {
	// Get the full executable path for the editor.
	executable, err := exec.LookPath(resolveEditor())
	if err != nil {
		return err
	}

	cmd := exec.Command(executable, resolveEditorArguments(executable, filename)...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func OpenFileInDefaultEditor(filename string) error {
	return OpenFileInEditor(filename, GetPreferredEditorFromEnvironment)
}

func CaptureInputFromEditor(fileResolver TempFileResolver) ([]byte, error) {
	file, err := fileResolver()
	if err != nil {
		return []byte{}, err
	}

	filename := file.Name()

	// Defer removal of the temporary file in case any of the next steps fail.
	defer os.Remove(filename)

	if err = file.Close(); err != nil {
		return []byte{}, err
	}

	if err = OpenFileInDefaultEditor(filename); err != nil {
		return []byte{}, err
	}

	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return []byte{}, err
	}

	return bytes, nil
}

func EmptyTempFile() (*os.File, error) {
	return ioutil.TempFile(os.TempDir(), "*")
}

func TempFileFrom(generate func() ([]byte, error)) TempFileResolver {
	return func() (*os.File, error) {
		file, err := EmptyTempFile()
		if err != nil {
			return file, err
		}

		contents, err := generate()
		if err != nil {
			return file, err
		}

		if _, err := file.Write(contents); err != nil {
			return file, err
		}

		return file, nil
	}
}
