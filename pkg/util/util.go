package util

import (
	"regexp"
	"strings"
)

var slugRE = regexp.MustCompile("[^a-z0-9]+")

func Slug(s string) string {
	return strings.Trim(slugRE.ReplaceAllString(strings.ToLower(s), "-"), "-")
}
