package commands

import (
	"fmt"

	"gitlab.com/marshall007/enhancements/pkg/repo"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var rootOpts = &repo.Options{}
var logLevel string

func New() *cobra.Command {
	cmd := &cobra.Command{
		Use:               "glep",
		Short:             "glep helps you build GitLab Enhancement Proposals",
		PersistentPreRunE: initLogging,
	}

	cmd.PersistentFlags().StringVar(
		&logLevel,
		"log-level",
		"info",
		fmt.Sprintf("the logging verbosity, either %s", logrus.AllLevels),
	)

	cmd.PersistentFlags().StringVar(
		&rootOpts.ProjectId,
		"project",
		repo.DefaultProjectID,
		"project ID of enhancement proposal repo",
	)

	cmd.PersistentFlags().StringVar(
		&rootOpts.TemplatePath,
		"template-path",
		repo.DefaultTemplatePath,
		"path to the enhancement proposal template",
	)

	cmd.PersistentFlags().StringVar(
		&rootOpts.ProposalPath,
		"proposal-path",
		repo.DefaultProposalDir,
		"path to the root enhancement proposal directory",
	)

	addCommands(cmd)
	return cmd
}

func addCommands(topLevel *cobra.Command) {
	addCreate(topLevel)
}

func initLogging(*cobra.Command, []string) error {
	lvl, err := logrus.ParseLevel(logLevel)
	if err == nil {
		return err
	}

	logrus.SetLevel(lvl)
	return nil
}
