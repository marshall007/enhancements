package commands

import (
	"fmt"
	"log"

	"gitlab.com/marshall007/enhancements/pkg/api"
	"gitlab.com/marshall007/enhancements/pkg/repo"
	"gitlab.com/marshall007/enhancements/pkg/util"

	"golang.org/x/exp/maps"
	"golang.org/x/exp/slices"

	"github.com/pkg/browser"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

func addCreate(topLevel *cobra.Command) {
	var openWebIDE bool

	e := api.Enhancement{}

	cmd := &cobra.Command{
		Use:           "create",
		Short:         "Create a new enhancement proposal",
		SilenceUsage:  true,
		SilenceErrors: true,
		PreRunE: func(cmd *cobra.Command, args []string) error {
			return e.Validate(args)
		},
		RunE: func(*cobra.Command, []string) error {
			return runCreate(&e, openWebIDE)
		},
	}

	cmd.PersistentFlags().StringVar(&e.Title, "title", "", "Title")
	cmd.PersistentFlags().IntVar(&e.Number, "number", 0, "proposal number (defaults to next MR id)")
	cmd.PersistentFlags().StringArrayVar(&e.Authors, "authors", []string{}, "author(s)")
	cmd.PersistentFlags().StringVar(&e.OwningStage, "owning-stage", "", fmt.Sprintf("product stage that owns the proposal (one of: %s)", api.AllStages))
	cmd.PersistentFlags().StringArrayVar(&e.ParticipatingStages, "participating-stages", []string{}, "other participant product stages")
	cmd.PersistentFlags().StringVar(&e.Status, "status", string(api.ProposedStatus), fmt.Sprintf("status (one of: %s)", api.AllStatuses))
	cmd.PersistentFlags().BoolVar(&openWebIDE, "open-editor", true, "automatically open link to edit the generated proposal")

	topLevel.AddCommand(cmd)
}

func runCreate(e *api.Enhancement, autoOpen bool) error {
	repo := repo.New(rootOpts)

	mr, err := createProposal(repo, e)
	if err != nil {
		return err
	}

	editLink := repo.WebIDELink(mr.IID)

	log.Printf("View MR: %s\n", mr.WebURL)
	log.Printf("View Branch: %s\n", repo.BranchesLink(mr.SourceBranch))
	log.Printf("Edit in WebIDE: %s\n", editLink)

	if autoOpen {
		browser.OpenURL(editLink.String())
	}

	return err
}

func createProposal(repo *repo.Repo, e *api.Enhancement) (*gitlab.MergeRequest, error) {
	validUsers := maps.Keys(repo.GetUsers(e.Authors))
	for _, a := range e.Authors {
		if !slices.Contains(validUsers, a) {
			return nil, fmt.Errorf("invalid username: %s", a)
		}
	}

	e.Authors = validUsers

	var mr *gitlab.MergeRequest
	var err error

	if e.Number == 0 {
		mr, err = repo.CreatePlaceholderMR(e.Title)
	} else {
		mr, err = repo.GetExistingMR(e.Number)
	}

	if err != nil {
		return nil, err
	}

	e.Number = mr.IID

	contents, err := util.CaptureInputFromEditor(
		util.TempFileFrom(func() ([]byte, error) {
			return repo.PopulateTemplate(e)
		}),
	)

	if err != nil {
		return nil, err
	}

	return mr, repo.WriteProposal(mr.SourceBranch, e.Filepath(), string(contents))
}
