package api

// https://about.gitlab.com/handbook/product/categories/#devops-stages
type ProductStage string

const (
	ConfigureStage  ProductStage = "configure"
	CreateStage     ProductStage = "create"
	ManageStage     ProductStage = "manage"
	MonitorStage    ProductStage = "monitor"
	PackageStage    ProductStage = "package"
	PlanStage       ProductStage = "plan"
	ProtectStage    ProductStage = "protect"
	ReleaseStage    ProductStage = "release"
	SecureStage     ProductStage = "secure"
	VerifyStage     ProductStage = "verify"
	EnablementStage ProductStage = "enablement"
)

var AllStages = []ProductStage{
	ConfigureStage,
	CreateStage,
	ManageStage,
	MonitorStage,
	PackageStage,
	PlanStage,
	ProtectStage,
	ReleaseStage,
	SecureStage,
	VerifyStage,
	EnablementStage,
}

type Status string

const (
	ProposedStatus      Status = "proposed"
	ImplementableStatus Status = "implementable"
	ImplementedStatus   Status = "implemented"
	DeferredStatus      Status = "deferred"
	RejectedStatus      Status = "rejected"
)

var AllStatuses = []Status{
	ProposedStatus,
	ImplementableStatus,
	ImplementedStatus,
	DeferredStatus,
	RejectedStatus,
}

type Milestone struct {
	Alpha      string `yaml:"alpha,omitempty"`
	Beta       string `yaml:"beta,omitempty"`
	Stable     string `yaml:"stable,omitempty"`
	Deprecated string `yaml:"deprecated,omitempty"`
	Removed    string `yaml:"removed,omitempty"`
}
