package api

import (
	"fmt"
	"path/filepath"

	"gitlab.com/marshall007/enhancements/pkg/util"

	"golang.org/x/exp/slices"
)

type Enhancement struct {
	Number              int       `yaml:"number"`
	Title               string    `yaml:"title"`
	OwningStage         string    `yaml:"owning-stage"`
	ParticipatingStages []string  `yaml:"participating-stages"`
	Status              string    `yaml:"status"`
	Authors             []string  `yaml:"authors"`
	Milestones          Milestone `yaml:"milestones,omitempty"`
}

func (e *Enhancement) Validate(args []string) error {
	stages := []string{}
	stages = append(stages, e.OwningStage)
	stages = append(stages, e.ParticipatingStages...)

	for _, s := range stages {
		if !slices.Contains(AllStages, ProductStage(s)) {
			return fmt.Errorf("invalid stage: %s", s)
		}
	}

	if !slices.Contains(AllStatuses, Status(e.Status)) {
		return fmt.Errorf("invalid status: %s", e.Status)
	}

	return nil
}

func (e *Enhancement) Name() string {
	return fmt.Sprintf("%04d-%s", e.Number, util.Slug(e.Title))
}

func (e *Enhancement) Filepath() string {
	return filepath.Join(
		string(e.OwningStage),
		fmt.Sprintf("%s.md", e.Name()),
	)
}
