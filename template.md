---
status: proposed
creation-date: yyyy-mm-dd
authors: [ "@username" ]
coach: "@username"
owning-section: "~section::<section>"
participating-sections: []
approvers: [ "@product-manager", "@engineering-manager" ]
---

<!--
**Note:** Please remove comment blocks for sections you've filled in.
When your blueprint is complete, all of these comment blocks should be removed.

To get started with this template:

- **Fill out this file as best you can.**
  At minimum, you should fill in the "Summary", and "Motivation" sections.
  These should be easy, espectially if the initiative is already on Product's
  roadmap or you've preflighted the idea of the blueprint in a Working Group.
- **Create a MR for this blueprint.**
  Assign it to an Architecture Evolution Coach (i.e. a Principal+ engineer).
- **Merge early and iterate.**
  Avoid getting hung up on specific details and instead aim to get the goals of
  the blueprint clarified and merged quickly. The best way to do this is to just
  start with the high-level sections and fill out details incrementally in
  subsequent MRs.

Just because a blueprint is merged does not mean it is complete or approved. Any blueprint
marked as a `proposed` is a working document and subject to change.

When editing blueprints, aim for tightly-scoped, single-topic MRs to keep discussions
focused. If you disagree with what is already in a document, open a new MR
with suggested changes.

If there are new details that belong in the blueprint, edit the blueprint. Once a
feature has become "implemented", major changes should get new blueprints.

The canonical place for the latest set of instructions (and the likely source
of this file) is [here](/template.md).
-->

# {+ Title of Blueprint +}

<!--
This is the title of your blueprint. Keep it short, simple, and descriptive. A good
title can help communicate what the blueprint is and should be considered as part of
any review.
-->

[[_TOC_]]

## Summary

<!--
This section is incredibly important for producing high quality user-focused
documentation such as release notes or a development roadmap. It should be
possible to collect this information before implementation begins in order to
avoid requiring implementors to split their attention between writing release
notes and implementing the feature itself.

A good summary is probably at least a paragraph in length.

Both in this section and below, follow the guidelines of the
[documentation style guide](https://docs.gitlab.com/ee/development/documentation/styleguide/).
In particular, wrap lines to a reasonable length, to make it easier for
reviewers to cite specific portions, and to minimize diff churn on updates.
-->

## Motivation

<!--
This section is for explicitly listing the motivation, goals and non-goals of
this blueprint. Describe why the change is important and the benefits to users.

The motivation section can optionally provide links to issues that demonstrate
interest in a blueprint within the wider GitLab community. Links to documentation
for competing products and services is also encouraged in cases where they
demonstrate clear gaps in the functionality GitLab provides.

For concrete proposals we recommend laying out goals and non-goals explicitly, but
this section may be framed in terms of problem statements, challenges, or opportunities.
The latter may be a more suitable framework in cases where the problem is not well-defined
or design details not yet established.
-->

### Goals

<!--
List the specific goals of the blueprint.
- What is it trying to achieve?
- How will we know that this has succeeded?
-->

### Non-Goals

<!--
Listing non-goals helps to focus discussion and make progress.
- What is out of scope for this blueprint?
-->

## Proposal

<!--
This is where we get down to the specifics of what the proposal actually is, but keep it simple!
This should have enough detail that reviewers can understand exactly what you're proposing, but should not include
things like API designs or implementation. The "Design Details" section below is for the real nitty-gritty.
-->

## Design Details

<!--
This section should contain enough information that the specifics of your change are understandable.
This may include API specs (though not always required) or even code snippets. If there's any ambiguity about HOW your
proposal will be implemented, this is the place to discuss them.

If it's helpful to include workflow diagrams or any other related images. Diagrams authored in GitLab flavored markdown
are prefered. In cases where that is not feasible, images should be placed under `images/` in the same directory
as the README.md for the proposal.
-->
